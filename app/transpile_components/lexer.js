(function(trans) {

	
	/**
   * Это лексический анализатор. Здесь должен быть ваш код вместо данной затычки!
   * @param {string} inputCode - строка с кодом программы на входном языке
   * @return {Array} - массив распознанных лексем
   */
  trans.TransProto.lexer = function (inputCode) {
    this.str_cnt = 0;
	this.pos_int_str = 0;
	
	while(inputCode.length != 0)
	{	
		var f = false;
		var max_eq = 0;
		var max_eq_type = "";
		for(var token_type in this.lexTable)
		{
			var reg = new RegExp(this.lexTable[token_type].regex);
			
			var ex_res = reg.exec(inputCode);
			if(ex_res)
			{
				if(ex_res[0].length > max_eq)
				{
					f = true;
					max_eq = ex_res[0].length;
					max_eq_type = token_type;
				}
			}
		}
	
		if(!f)
		{
			return {
				"status" : "Error",
				"error" :
				{
					"error_text" : "Ошибка в строке " + (this.str_cnt + 1) + '. Не найдено совпадений в таблице',
					"prog_text" : inputCode,
					"line" : this.str_cnt + 1
				}
			};
		}
		else
		{
			var reg = new RegExp(this.lexTable[max_eq_type].regex);
			var match = reg.exec(inputCode)[0];
			var obj = this.lexTable[max_eq_type];
			this.is_list = function(obj2, match)
			{
				var list = obj2.list;
				if(list)
				{
					for(var key in list)
					{
						for(var i = 0; i < list[key].length; i++)
						{
							if(list[key][i] == match)
							{
								return {
									"type" : max_eq_type,
									"group" : key,
									"match" : match
								};
							}
						}
					}
				}
				//Если совпадений не найдено, то считать это переменной/названием функции/константой
				for(token_type in this.lexTable)
				{
					if(this.lexTable[token_type].link)
					{
						var reg = new RegExp(this.lexTable[token_type].regex);
			
						var ex_res = reg.exec(inputCode);
						if(ex_res)
						{
							max_eq_type = token_type;
							obj = this.lexTable[max_eq_type];
						}
					}
				}
				return false;
			};
			this.is_link = function(obj, match, name)
			{
				if(obj.link)
				{
					var l = obj.link.length;
					for(var i = 0; i < l; i++)
					{
						if(obj.link[i] == match)
						{
							return	{
								"type" : name,
								"value" : obj.link[i]
							};
						}
					}
					obj.link.push(match);
					return {
						"type" : name,
						"value" : obj.link[l]
 					};
				}
				return false;
			};
			this.is_skiped = function(obj)
			{
				if(obj.skip)
				{
					return true;
				}
				else
				{
					return false;
				}
			};
			var lexem = this.is_skiped(obj) || this.is_list(obj, match) || this.is_link(obj, match, max_eq_type);
			if(!lexem)
			{
				return {
					"status" : "Error",
					"error" :
					{
						"error_text" : "Ошибка в строке " + (this.str_cnt + 1) + '. Не определен тип лексемы',
						"token" : max_eq_type,
						"match" : match,
						"line" : this.str_cnt + 1
					}
				};			
			}
			else
			{
				if(!(lexem === true))
				{
					lexem["string"] = this.str_cnt + 1;
					lexem["position"] = this.pos_int_str;
					this.lexArray.push(lexem);
				}
				
				for(var i = 0; i < match.length; i++)
				{
					if(match[i] === "\n")
					{
						++this.str_cnt;
						this.pos_int_str = 0;
					}
					else
					{
						this.pos_int_str++;
					}
				}
				inputCode = inputCode.replace(reg, "");
			}
		}

	}
	
	return {
		"status" : "Ok",
		"result" :
		{
			"idents" : this.arrayOfSymbols,
			"lexems" : this.lexArray
		}
	};
	
	//return this.lexArray;
  }
  
  

})(window.trans || (window.trans = {}));